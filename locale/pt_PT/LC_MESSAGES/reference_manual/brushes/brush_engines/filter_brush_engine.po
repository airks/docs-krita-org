msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:37+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: optionsize optionopacitynflow icons Krita image\n"
"X-POFile-SpellExtra: optionrotation filterbrush optionbrushtip\n"
"X-POFile-SpellExtra: blendingmodes images ref optionmirror\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:1
msgid "The Filter Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis de Filtragem."

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:17
msgid "Filter Brush Engine"
msgstr "Motor de Pincéis de Filtragem"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:20
msgid ".. image:: images/icons/filterbrush.svg"
msgstr ".. image:: images/icons/filterbrush.svg"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:21
msgid ""
"Where in other programs you have a 'dodge tool', 'blur tool' and 'sharpen "
"tool', Krita has a special brush engine for this: The Filter Brush engine. "
"On top of that, due to Krita's great integration of the filters, a huge "
"amount of filters you'd never thought you wanted to use for a drawing are "
"possible in brush form too!"
msgstr ""
"Onde existe nos outros programas uma 'ferramenta de desvio', de 'borrão' ou "
"de 'afiamento' (nitidez, o Krita tem um motor de pincéis especiais para esse "
"fim: o motor de Pincéis de Filtragem. Assente sobre isso, derivado da óptima "
"integração do Krita com os filtros, é possível usar uma quantidade enorme de "
"filtros que nunca pensaria querer usar sobre um desenho da mesma forma!"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:24
msgid "Options"
msgstr "Opções"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:26
msgid "The filter brush has of course some basic brush-system parameters:"
msgstr ""
"O pincel de filtragem tem obviamente alguns parâmetros básicos do sistema de "
"pincéis:"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:28
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:31
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:32
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"
