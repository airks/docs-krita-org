# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:31+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en colorcategory image KikicLUTprofiles MediaWiki\n"
"X-POFile-SpellExtra: Kiki images Sphinx alt\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Imagem do Kiki confuso no meio dos livros."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "Ficheiro Não Encontrado (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "Esta página não existe."

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "Isto poder-se-á dever ao seguinte:"

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"Foi movido o manual do MediaWiki para o Sphinx e, com isto, vieram muitas "
"reorganizações."

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "A página foi descontinuada."

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "Pode ter cometido um erro de escrita do URL."

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
"Em qualquer dos casos, conseguirá encontrar a página que anda à procura, se "
"usar o campo de pesquisa na navegação do lado esquerdo."
