# Translation of docs_krita_org_reference_manual___render_animation.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___render_animation\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/render_animation.rst:1
msgid "How to use the render animation command in Krita."
msgstr "Як користуватися командою обробки анімації у Krita."

#: ../../reference_manual/render_animation.rst:12
#: ../../reference_manual/render_animation.rst:17
#: ../../reference_manual/render_animation.rst:42
msgid "Render Animation"
msgstr "Обробка анімації"

#: ../../reference_manual/render_animation.rst:12
msgid "Animation"
msgstr "Анімація"

#: ../../reference_manual/render_animation.rst:19
msgid ""
"Render animation allows you to render your animation to an image sequence, "
"gif, mp4, mkv, or ogg file. It replaces :guilabel:`Export Animation`."
msgstr ""
"Засіб обробки анімації надає вам змогу зберегти вашу анімацію як "
"послідовність зображень, файл gif, mp4, mkv або ogg. Цей засіб замінив собою "
"пункт меню :guilabel:`Експортувати анімацію`."

#: ../../reference_manual/render_animation.rst:21
msgid ""
"For rendering to an animated file format, Krita will first render to a png "
"sequence and then use FFmpeg, which is really good at encoding into video "
"files, to render that sequence to an animated file format. The reason for "
"this two-step process is that animation files can be really complex and "
"really big, and this is the best way to allow you to keep control over the "
"export process. For example, if your computer has a hiccup, and one frame "
"saves out weird, first saving the image sequence allows you to only resave "
"that one weird frame before rendering."
msgstr ""
"Щоб створити файл у форматі анімації, Krita спочатку створює послідовність "
"кадрів у форматі png, а потім використовує бібліотеку FFmpeg, яка дуже добре "
"працює у кодуванні відеофайлів, для перетворення послідовності кадрів на "
"файл у форматі анімації. Причина такого двокрокового процесу перетворення "
"полягає у тому, що файли анімації можуть бути дуже складними і дуже "
"великими, а двокроковий процес є найкращим способом зберегти за вами повний "
"контроль над процесом експортування. Наприклад, якщо у комп'ютера виникли "
"проблеми і один з кадрів було збережено з помилками, збереження до "
"послідовності зображень надасть вам змогу виправити цей помилковий кадр без "
"повторного створення усієї послідовності анімації."

#: ../../reference_manual/render_animation.rst:23
msgid ""
"This means that you will need to find a good place to stick your frames "
"before you can start rendering. If you only do throwaway animations, you can "
"use a spot on your hard-drive with enough room and select :guilabel:`Delete "
"Sequence After Rendering`."
msgstr ""
"Це означає, що вам слід знайти місце для зберігання кадрів, перш ніж "
"програма почне збирати з них анімацію. Якщо це лише прохідна анімація, ви "
"можете скористатися якимось жорстким диском, де достатньо вільного місця, і "
"позначити пункт :guilabel:`Вилучити послідовність після обробки`."

#: ../../reference_manual/render_animation.rst:26
msgid "Image Sequence"
msgstr "Послідовність зображень"

#: ../../reference_manual/render_animation.rst:28
msgid "Base Name"
msgstr "Базова назва"

#: ../../reference_manual/render_animation.rst:29
msgid ""
"The base name of your image sequence. This will get suffixed with a number "
"depending on the frame."
msgstr ""
"Базова назва вашої послідовності зображень. До цієї назви буде додано суфікс "
"номера кадру."

#: ../../reference_manual/render_animation.rst:30
msgid "File Format"
msgstr "Формат файла"

#: ../../reference_manual/render_animation.rst:31
msgid ""
"The file format to export the sequence to. When rendering we enforce png. "
"The usual export options can be modified with :guilabel:`...`."
msgstr ""
"Формат файлів для експортування послідовності зображень. Під час обробки "
"буде примусово використано формат png. Змінити параметри експортування можна "
"натисканням кнопки :guilabel:`...`."

#: ../../reference_manual/render_animation.rst:32
msgid "Render Location"
msgstr "Місце обробки"

#: ../../reference_manual/render_animation.rst:33
msgid ""
"Where you render the image sequence to. Some people prefer to use a flash-"
"drive or perhaps a harddrive that is fast."
msgstr ""
"Місце, куди слід зберегти послідовність зображень. Дехто надає перевагу "
"флешці або швидкому диску."

#: ../../reference_manual/render_animation.rst:34
msgid "First Frame"
msgstr "Перший кадр"

#: ../../reference_manual/render_animation.rst:35
msgid ""
"The first frame of the range of frames you wish to adjust. Automatically set "
"to the first frame of your current selection in the timeline. This is useful "
"when you only want to re-render a little part."
msgstr ""
"Перший кадр з діапазону кадрів, які ви хочете скоригувати. Автоматично "
"встановлюється на перший кадр поточного позначеного на монтажному столі "
"набору. Корисно, якщо ви хочете повторно обробити лише малу частину кадрів."

#: ../../reference_manual/render_animation.rst:36
msgid "Last Frame"
msgstr "Останній кадр"

#: ../../reference_manual/render_animation.rst:37
msgid ""
"As above, the last frame of the range of frames you wish to adjust. "
"Automatically set to the last frame of your current selection in the "
"timeline."
msgstr ""
"Як і вище, останній кадр діапазону кадрів, які ви хочете скоригувати. "
"Автоматично встановлюється на останній кадр поточного позначеного на "
"монтажному столі набору кадрів."

#: ../../reference_manual/render_animation.rst:39
msgid "Naming Sequence starts with"
msgstr "Назви файлів у послідовності починаються з"

#: ../../reference_manual/render_animation.rst:39
msgid ""
"The frames are named by using :guilabel:`Base Name`  above and adding a "
"number for the frame. This allows you to set where the frame number starts, "
"so rendering from 8 to 10 with starting point 3 will give you images named "
"11 and 15. Useful for programs that don't understand sequences starting with "
"0, or for precision output."
msgstr ""
"Назви кадрів визначатимуться на основі значення :guilabel:`Базова назва`, "
"визначеного вище, із додаванням номера кадру. За допомогою цього пункту ви "
"можете визначити початок нумерації кадрів. Отже, обробка від 8 до 10 із "
"початковим пунктом 3 дасть вам зображення із номерами у назвах від 11 до 15. "
"Корисно для програм, які не можуть обробляти послідовності кадрів, які "
"починаються з 0, або для уточнення виведення даних."

#: ../../reference_manual/render_animation.rst:44
msgid "Render As"
msgstr "Обробити як"

#: ../../reference_manual/render_animation.rst:45
msgid ""
"The file format to render to. All except gif have extra options that can be "
"manipulated via :guilabel:`...`."
msgstr ""
"Формат файлів для обробки. Для усіх форматів, окрім gif, передбачено "
"додаткові параметри, змінити які можна натисканням кнопки :guilabel:`...`."

#: ../../reference_manual/render_animation.rst:46
msgid "File"
msgstr "Файл"

#: ../../reference_manual/render_animation.rst:47
msgid "Location and name of the rendered animation."
msgstr "Розташуванні назва файла оброблених даних анімації."

#: ../../reference_manual/render_animation.rst:48
msgid "FFmpeg"
msgstr "FFmpeg"

#: ../../reference_manual/render_animation.rst:49
msgid ""
"The location where your have FFmpeg. If you don't have this, Krita cannot "
"render an animation. For proper gif support, you will need FFmpeg 2.6, as we "
"use its palettegen functionality."
msgstr ""
"Місце, куди встановлено FFmpeg. Якщо програму не встановлено у системі Krita "
"не зможе обробляти анімації. Щоб мати належну підтримку створення gif, вам "
"слід встановити FFmpeg 2.6, оскільки ми використовуємо функціональну "
"можливість palettegen з цієї і наступних версій."

#: ../../reference_manual/render_animation.rst:51
msgid "Delete Sequence After Rendering"
msgstr "Вилучити послідовність після обробки"

#: ../../reference_manual/render_animation.rst:51
msgid ""
"Delete the prerendered image sequence after done rendering. This allows you "
"to choose whether to try and save some space, or to save the sequence for "
"when encoding fails."
msgstr ""
"Вилучити попередньо оброблену послідовність зображень після створення файла "
"анімації. За допомогою цього пункту ви зможете визначити, слід програмі "
"вилучити зайві дані для заощадження вільного місця на диску чи зберегти "
"послідовність зображень на випадок, якщо спроба створити файл анімації "
"зазнає невдачі."

#: ../../reference_manual/render_animation.rst:55
msgid ""
"None of the video formats support saving from images with a transparent "
"background, so Krita will try to fill it with something. You should add a "
"background color yourself to avoid it from using, say, black."
msgstr ""
"У жодному з форматів, у якому може зберігати дані програма, не передбачено "
"можливості зберігати анімацію із прозорим тлом, тому Krita намагатиметься "
"заповнити тло якимось кольором. Вам слід додати колір тла вручну, якщо ви не "
"хочете, щоб програма використала, скажімо, чорний."

#: ../../reference_manual/render_animation.rst:58
msgid "Setting Up Krita for Exporting Animations"
msgstr "Налаштовування Krita на експортування анімацій"

#: ../../reference_manual/render_animation.rst:60
msgid ""
"You will need to download an extra application and link it in Krita for it "
"to work. The application is pretty big (50MB), so the Krita developers "
"didn't want to bundle it with the normal application. The software that we "
"will use is free and called FFmpeg. The following instructions will explain "
"how to get it and set it up. The setup is a one-time thing so you won't have "
"to do it again."
msgstr ""
"Вам доведеться отримати пакунок додаткової програми і скомпонувати її з "
"Krita, щоб можна було скористатися цим пунктом. Пакунок з програмою є доволі "
"великим (50 МБ), тому розробники Krita не включили її до звичайного пакунка "
"Krita. Використане нами програмне забезпечення є вільним і має назву FFmpeg. "
"Нижче наведено настанови щодо отримання цього програмного забезпечення та "
"його налаштовування. Налаштовування доведеться виконати лише одного разу — "
"потреби у повторному налаштовуванні не буде."

#: ../../reference_manual/render_animation.rst:63
msgid "Step 1 - Downloading FFmpeg"
msgstr "Крок 1 — отримання FFmpeg"

#: ../../reference_manual/render_animation.rst:66
#: ../../reference_manual/render_animation.rst:86
msgid "For Windows"
msgstr "У Windows"

#: ../../reference_manual/render_animation.rst:68
msgid ""
"Go to the `FFmpeg website <https://ffmpeg.org/download.html>`_. The URL that "
"had the link for me was `here... <https://ffmpeg.zeranoe.com/builds/>`_"
msgstr ""
"Перейдіть на `сайт FFmpeg <https://ffmpeg.org/download.html>`_. Адреса "
"пакунків для Windows — <https://ffmpeg.zeranoe.com/builds/>`_"

#: ../../reference_manual/render_animation.rst:70
msgid ""
"Watch out for the extremely annoying google and that looks like a download "
"button! There is no big button for what we need. Either get the 64-bit "
"STATIC version or 32-bit STATIC version that is shown later down the page. "
"If you bought a computer in the past 5 years, you probably want the 64-bit "
"version. Make sure you get a exe file, if you hover over the options they "
"will give more information about what exactly you are downloading."
msgstr ""
"Не звертайте увагу на рекламу, яка виглядає як кнопка отримання пакунка "
"(«Download»)! Нам не потрібна ця велика кнопка. Нам потрібна 64-бітова "
"статична (STATIC) версія пакунка або 32-бітова статична (STATIC) версія "
"пакунка, посилання на які розташовано нижче на сторінці. Якщо ви придбали "
"комп'ютер протягом останніх 5 років, вам, ймовірно, потрібна 64-бітова "
"версія. Вам потрібен файл exe. Якщо ви наведете вказівник миші на посилання, "
"програма має показати вам більше відомостей щодо того, що саме ви отримаєте."

#: ../../reference_manual/render_animation.rst:73
#: ../../reference_manual/render_animation.rst:93
msgid "For OSX"
msgstr "У OSX"

#: ../../reference_manual/render_animation.rst:75
msgid ""
"Please see the section above. However, FFmpeg is obtained from `here "
"<https://evermeet.cx/ffmpeg/>`_ instead. Just pick the big green button on "
"the left under the FFmpeg heading. You will also need an archiving utility "
"that supports .7z, since FFmpeg provides their OSX builds in .7z format. If "
"you don't have one, try something like `Keka <https://www.kekaosx.com>`_."
msgstr ""
"Будь ласка, ознайомтеся із розділом, наведеним вище. Крім того, FFmpeg можна "
"отримати `тут <https://evermeet.cx/ffmpeg/>`_. Просто натисніть велику "
"зелену кнопку ліворуч під заголовком FFmpeg. Вам також знадобиться програма "
"для роботи з архівами, у якій передбачено підтримку .7z, оскільки пакунки "
"FFmpeg для macOS збирають у форматі .7z. Якщо у вас немає такої програми, "
"спробуйте встановити `Keka <https://www.kekaosx.com>`_."

#: ../../reference_manual/render_animation.rst:78
#: ../../reference_manual/render_animation.rst:98
msgid "For Linux"
msgstr "У Linux"

#: ../../reference_manual/render_animation.rst:80
msgid ""
"FFmpeg can be installed from the repositories on most Linux systems. Version "
"2.6 is required for proper gif support, as we use the palettegen "
"functionality."
msgstr ""
"FFmpeg у більшості дистрибутивів Linux можна встановити зі сховищ пакунків. "
"Для забезпечення належної підтримки gif слід встановити принаймні версію "
"2.6, оскільки ми використовуємо можливості palettegen."

#: ../../reference_manual/render_animation.rst:83
msgid "Step 2 - Unzipping and Linking to Krita"
msgstr "Крок 2 — розпаковування та компонування з Krita"

#: ../../reference_manual/render_animation.rst:88
msgid ""
"Unzip the package that was just downloaded. Rename the long folder name to "
"just ffmpeg. Let's put this folder in a easy to find location. Go to your C:"
"\\ and place it there. You can put it wherever you want, but that is where I "
"put it."
msgstr ""
"Розпакуйте пакунок, який ви отримали. Перейменуйте довгу назву теки на "
"просто «ffmpeg». Пересуньте цю теку до якогось зручного місця. Наприклад, "
"перейдіть до вашого диска C:\\ і збережіть її там. Теку можна розташувати де "
"завгодно, але автор підручника зберіг її саме там."

#: ../../reference_manual/render_animation.rst:90
msgid ""
"Open Krita back up and go to :menuselection:`File --> Render Animation`. "
"Click the :guilabel:`Browse` button on the last item called FFmpeg. Select "
"this file ``C:/ffmpeg/bin/ffmpeg.exe`` and click :guilabel:`OK`."
msgstr ""
"Відкрийте вікно Krita і скористайтеся пунктом меню :menuselection:`Файл --> "
"Обробити анімацію`. Натисніть кнопку :guilabel:`Вибрати` на останньому "
"пункті, який має назву «FFmpeg». Виберіть файл ``C:/ffmpeg/bin/ffmpeg.exe`` "
"і натисніть кнопку :guilabel:`Гаразд` ."

#: ../../reference_manual/render_animation.rst:95
msgid ""
"After downloading FFmpeg, you just need to extract it and then simply point "
"to it in the FFmpeg location in Krita like ``/Users/user/Downloads/ffmpeg`` "
"(assuming you downloaded and extracted the .7z file to /Users/user/"
"Downloads)."
msgstr ""
"Після отримання FFmpeg вам достатньо видобути програму і вказати її "
"розташування, наприклад ``/Users/користувач/Downloads/ffmpeg`` Krita (тут ми "
"припускаємо, що ви отримали пакунок і видобули файли .7z до /Users/"
"користувач/Downloads)."

#: ../../reference_manual/render_animation.rst:100
msgid ""
"FFmpeg is, if installed from the repositories, usually found in ``/usr/bin/"
"ffmpeg``."
msgstr ""
"FFmpeg, якщо пакунок встановлено зі сховищ пакунків, зазвичай, "
"встановлюється як ``/usr/bin/ffmpeg``."

#: ../../reference_manual/render_animation.rst:103
msgid "Step 3 - Testing out an animation"
msgstr "Крок 3 — тестування анімації"

#: ../../reference_manual/render_animation.rst:105
msgid ""
"ffmpeg.exe is what Krita uses to do all of its animation export magic. Now "
"that it is hooked up, let us test it out."
msgstr ""
"Для виконання усіх дій із експортування анімації Krita використовує ffmpeg."
"exe. Коли ми встановлено усі потрібні компоненти, варто виконати тестування."

#: ../../reference_manual/render_animation.rst:107
msgid ""
"Let's make an animated GIF. In the Render Animation dialog, change the :"
"guilabel:`Render As`  field to \"GIF image\". Choose the file location where "
"it will save with the \"File\" menu below. I just saved it to my desktop and "
"called it \"export.gif\". When it is done, you should be able to open it up "
"and see the animation."
msgstr ""
"Створімо анімований файл GIF. У діалоговому вікні обробки анімації змініть "
"вміст поля :guilabel:`Обробити як` на «зображення GIF». Виберіть місце для "
"файла, куди програма збереже файл, та вкажіть його назву за допомогою поля "
"«Файл». Автор підручника просто зберіг файл на стільницю і назвав його "
"«export.gif». Коли обробку буде завершено, ви зможете відкрити файл і "
"переглянути анімацію."

#: ../../reference_manual/render_animation.rst:111
msgid ""
"By default, FFmpeg will render MP4 files with a too new codec, which means "
"that Windows Media Player won't be able to play it. So for Windows, select "
"\"baseline\" for the profile instead of \"high422\" before rendering."
msgstr ""
"Типово, FFmpeg створює файли MP4 за допомогою надто нової версії кодека, що "
"означає, що Windows Media Player не зможе відтворити такі файли. Отже, у "
"Windows виберіть «baseline» для профілю, замість «high422», перш ніж буде "
"розпочато обробку."

#: ../../reference_manual/render_animation.rst:115
msgid ""
"OSX does not come with any software to play MP4 and MKV files. If you use "
"Chrome for your web browser, you can drag the video file into that and the "
"video should play. Otherwise you will need to get a program like VLC to see "
"the video."
msgstr ""
"У типовій macOS немає програмного забезпечення для відтворення файлів MP4 та "
"MKV. Якщо ви використовуєте для перегляду сторінок інтернету Chrome, ви "
"можете перетягнути пункт файла анімації до вікна браузера. Chrome має почати "
"відтворення файла. Крім того, ви можете встановити програму-програвач, "
"наприклад VLC, для перегляду відео."

#~ msgid "FFMpeg"
#~ msgstr "FFMpeg"
