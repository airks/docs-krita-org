# Translation of docs_krita_org_reference_manual___blending_modes___darken.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-09-01 16:52+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../reference_manual/blending_modes/darken.rst:1
msgid ""
"Page about the darken blending modes in Krita: Darken, Burn, Easy Burn, Fog "
"Darken, Darker Color, Gamma Dark, Linear Burn and Shade."
msgstr ""
"Pàgina sobre els modes de barreja més foscos al Krita: Fosca, Cremat, Cremat "
"fàcil, Boira fosca, Color més fosc, Gamma fosca, Cremat lineal i Ombra."

#: ../../reference_manual/blending_modes/darken.rst:16
#: ../../reference_manual/blending_modes/darken.rst:75
#: ../../reference_manual/blending_modes/darken.rst:79
msgid "Darken"
msgstr "Fosca"

#: ../../reference_manual/blending_modes/darken.rst:18
msgid "Color Burn"
msgstr "Cremat del color"

#: ../../reference_manual/blending_modes/darken.rst:18
msgid "burn"
msgstr "Cremat"

#: ../../reference_manual/blending_modes/darken.rst:23
msgid "Burn"
msgstr "Cremat"

#: ../../reference_manual/blending_modes/darken.rst:25
msgid "A variation on Divide, sometimes called 'Color Burn' in some programs."
msgstr ""
"Una variació de Divideix, de vegades anomenada «Cremat del color» en alguns "
"programes."

#: ../../reference_manual/blending_modes/darken.rst:27
msgid ""
"This inverts the bottom layer, then divides it by the top layer, and inverts "
"the result. This results in a darkened effect that takes the colors of the "
"lower layer into account, similar to the burn technique used in traditional "
"darkroom photography."
msgstr ""
"Això inverteix la capa inferior, després la divideix per la capa superior i "
"inverteix el resultat. Això dóna com a resultat un efecte fosc que té en "
"compte els colors de la capa inferior, similar a la tècnica de cremat "
"emprada en la cambra fosca de la fotografia tradicional."

#: ../../reference_manual/blending_modes/darken.rst:30
msgid ""
"1_{[1_Darker Gray(0.4, 0.4, 0.4)] / Lighter Gray(0.5, 0.5, 0.5)} = (-0.2, "
"-0.2, -0.2) → Black(0, 0, 0)"
msgstr ""
"1_{[1_Gris fosc (0,4, 0,4, 0,4)] / Gris clar (0,5, 0,5, 0,5)} = (-0,2, -0,2, "
"-0,2) → Negre (0, 0, 0)"

#: ../../reference_manual/blending_modes/darken.rst:35
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:35
#: ../../reference_manual/blending_modes/darken.rst:42
#: ../../reference_manual/blending_modes/darken.rst:47
msgid "Left: **Normal**. Right: **Burn**."
msgstr "Esquerra: **Normal**. Dreta: **Cremat**."

#: ../../reference_manual/blending_modes/darken.rst:37
msgid ""
"1_{[1_Light Blue(0.1608, 0.6274, 0.8274)] / Orange(1, 0.5961, 0.0706)} = "
"(0.1608, 0.3749, -1.4448) → Green(0.1608, 0.3749, 0)"
msgstr ""
"1_{[1_Blau clar (0,1608, 0,6274, 0,8274)] / Taronja (1, 0,5961, 0,0706)} = "
"(0,1608, 0.3749, -1,4448) → Verd (0,1608, 0,3749, 0)"

#: ../../reference_manual/blending_modes/darken.rst:42
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:47
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:49
#: ../../reference_manual/blending_modes/darken.rst:53
msgid "Easy Burn"
msgstr "Cremat fàcil"

#: ../../reference_manual/blending_modes/darken.rst:55
msgid ""
"Aims to solve issues with Color Burn blending mode by using a formula which "
"falloff is similar to Dodge, but the falloff rate is softer. It is within "
"the range of 0.0f and 1.0f unlike Color Burn mode."
msgstr ""
"L'objectiu és resoldre els problemes amb el mode de barreja Cremat del "
"color, s'utilitza una fórmula en la qual el decaïment és similar a "
"Esvaïment, però la velocitat del decaïment és més suau. Es troba dintre de "
"l'interval de 0,0f i 1,0f a diferència del mode Cremat del color."

#: ../../reference_manual/blending_modes/darken.rst:60
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Easy_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Easy_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:60
msgid "Left: **Normal**. Right: **Easy Burn**"
msgstr "Esquerra: **Normal**. Dreta: **Cremat fàcil**."

#: ../../reference_manual/blending_modes/darken.rst:62
msgid "Fog Darken"
msgstr "Boira fosca"

#: ../../reference_manual/blending_modes/darken.rst:66
msgid "Fog Darken (IFS Illusions)"
msgstr "Boira fosca (Il·lusions IFS)"

#: ../../reference_manual/blending_modes/darken.rst:68
msgid ""
"Darken the image in a way that there is a 'fog' in the end result. This is "
"due to the unique property of fog darken in which midtones combined are "
"lighter than non-midtones blend."
msgstr ""
"Enfosqueix la imatge de manera que hi hagi una «boira» al resultat final. "
"Això es deu a la propietat única d'enfosquiment que té la boira, en la qual "
"els tons mitjans combinats són més clars que la barreja de tons que no són "
"mitjans."

#: ../../reference_manual/blending_modes/darken.rst:73
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Fog_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Fog_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:73
msgid "Left: **Normal**. Right: **Fog Darken** (exactly the same as Addition)."
msgstr ""
"Esquerra: **Normal**. Dreta: **Boira fosca** (exactament el mateix que "
"Addició)."

#: ../../reference_manual/blending_modes/darken.rst:81
msgid ""
"With the darken, the upper layer's colors are checked for their lightness. "
"Only if they are darker than the underlying color on the lower layer, will "
"they be visible."
msgstr ""
"Amb l'enfosquiment, els colors de la capa superior es verifiquen per la seva "
"claredat. Només seran visibles si són més foscos que el color subjacent a la "
"capa inferior."

#: ../../reference_manual/blending_modes/darken.rst:83
msgid ""
"Is Lighter Gray(0.5, 0.5, 0.5) darker than Darker Gray(0.4, 0.4, 0.4)? = "
"(no, no, no) → Darker Gray(0.4, 0.4, 0.4)"
msgstr ""
"És el gris clar (0,5, 0,5, 0,5) més fosc que el gris fosc (0,4, 0,4, 0,4)? = "
"(no, no, no) → Gris fosc (0,4, 0,4, 0,4)"

#: ../../reference_manual/blending_modes/darken.rst:88
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:88
#: ../../reference_manual/blending_modes/darken.rst:95
#: ../../reference_manual/blending_modes/darken.rst:100
msgid "Left: **Normal**. Right: **Darken**."
msgstr "Esquerra: **Normal**. Dreta: **Fosca**."

#: ../../reference_manual/blending_modes/darken.rst:90
msgid ""
"Is Orange(1, 0.5961, 0.0706) darker than Light Blue(0.1608, 0.6274, 0.8274)? "
"= (no, yes, yes) → Green(0.1608, 0.5961, 0.0706)"
msgstr ""
"És el taronja (1, 0,5961, 0,0706) més fosc que el blau clar (0,1608, 0,6274, "
"0,8274)? = (no, sí, sí) → Verd (0,1608, 0,5961, 0,0706)"

#: ../../reference_manual/blending_modes/darken.rst:95
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:100
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:102
#: ../../reference_manual/blending_modes/darken.rst:106
msgid "Darker Color"
msgstr "Color més fosc"

#: ../../reference_manual/blending_modes/darken.rst:111
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darker_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darker_Color_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:111
msgid "Left: **Normal**. Right: **Darker Color**."
msgstr "Esquerra: **Normal**. Dreta: **Color més fosc**."

#: ../../reference_manual/blending_modes/darken.rst:113
#: ../../reference_manual/blending_modes/darken.rst:117
msgid "Gamma Dark"
msgstr "Gamma fosca"

#: ../../reference_manual/blending_modes/darken.rst:119
msgid ""
"Divides 1 by the upper layer, and calculates the end result using that as "
"the power of the lower layer."
msgstr ""
"Divideix 1 per la capa superior i calcula el resultat final utilitzant-lo "
"com la potència de la capa inferior."

#: ../../reference_manual/blending_modes/darken.rst:121
msgid ""
"Darker Gray(0.4, 0.4, 0.4)^[1 / Lighter Gray(0.5, 0.5, 0.5)] = Even Darker "
"Gray(0.1600, 0.1600, 0.1600)"
msgstr ""
"Gris fosc (0,4, 0,4, 0,4)^[1 / Gris clar (0,5, 0,5, 0,5)] = Gris encara més "
"fosc (0,1600, 0,1600, 0,1600)"

#: ../../reference_manual/blending_modes/darken.rst:126
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:126
#: ../../reference_manual/blending_modes/darken.rst:133
#: ../../reference_manual/blending_modes/darken.rst:138
msgid "Left: **Normal**. Right: **Gamma Dark**."
msgstr "Esquerra: **Normal**. Dreta: **Gamma fosca**."

#: ../../reference_manual/blending_modes/darken.rst:128
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274)^[1 / Orange(1, 0.5961, 0.0706)] = "
"Green(0.1608, 0.4575, 0.0683)"
msgstr ""
"Blau clar (0,1608, 0,6274, 0,8274)^[1 / Taronja (1, 0,5961, 0,0706)] = Verd "
"(0,1608, 0,4575, 0,0683)"

#: ../../reference_manual/blending_modes/darken.rst:133
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:138
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:140
#: ../../reference_manual/blending_modes/darken.rst:144
msgid "Linear Burn"
msgstr "Cremat lineal"

#: ../../reference_manual/blending_modes/darken.rst:146
msgid ""
"Adds the values of the two layers together and then subtracts 1. Seems to "
"produce the same result as :ref:`bm_inverse_subtract`."
msgstr ""
"Afegeix els valors de les dues capes i després resta 1. Sembla que produeix "
"el mateix resultat que :ref:`bm_inverse_subtract`."

#: ../../reference_manual/blending_modes/darken.rst:148
msgid ""
"[Darker Gray(0.4, 0.4, 0.4) + Lighter Gray(0.5, 0.5, 0.5)]_1 = (-0.1000, "
"-0.1000, -0.1000)  → Black(0, 0, 0)"
msgstr ""
"[Gris fosc (0,4, 0,4, 0,4) + Gris clar (0,5, 0,5, 0,5)]_1 = (-0,1000, "
"-0,1000, -0,1000) → Negre (0, 0, 0)"

#: ../../reference_manual/blending_modes/darken.rst:153
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/darken.rst:153
#: ../../reference_manual/blending_modes/darken.rst:160
#: ../../reference_manual/blending_modes/darken.rst:165
msgid "Left: **Normal**. Right: **Linear Burn**."
msgstr "Esquerra: **Normal**. Dreta: **Cremat lineal**."

#: ../../reference_manual/blending_modes/darken.rst:155
msgid ""
"[Light Blue(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706)]_1 = "
"(0.1608, 0.2235, -0.1020) → Dark Green(0.1608, 0.2235, 0)"
msgstr ""
"[Blau clar (0,1608, 0,6274, 0,8274) + Taronja (1, 0,5961, 0,0706)]_1 = "
"(0,1608, 0,2235, -0,1020) → Verd fosc (0,1608, 0,2235, 0)"

#: ../../reference_manual/blending_modes/darken.rst:160
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:165
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:167
msgid "Shade"
msgstr "Ombra"

#: ../../reference_manual/blending_modes/darken.rst:171
msgid "Shade (IFS Illusions)"
msgstr "Ombra (Il·lusions IFS)"

#: ../../reference_manual/blending_modes/darken.rst:173
msgid ""
"Basically, the blending mode only ends in shades of shades. This means that "
"it's very useful for painting shading colors while still in the range of "
"shades."
msgstr ""
"Bàsicament, el mode de barreja només finalitza a les ombres de les ombres. "
"Això vol dir que és molt útil per a pintar colors d'ombrejat mentre es troba "
"dins d'un cert interval d'ombres."

#: ../../reference_manual/blending_modes/darken.rst:179
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Shade_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Shade_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:179
msgid "Left: **Normal**. Right: **Shade**."
msgstr "Esquerra: **Normal**. Dreta: **Ombra**."
