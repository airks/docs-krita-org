# Translation of docs_krita_org_reference_manual___tools___path.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:11+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Activa l'ajust de l'angle"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../<rst_epilog>:34
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: eina de corbes de Bézier"

#: ../../reference_manual/tools/path.rst:1
msgid "Krita's path tool reference."
msgstr "Referència de l'eina Camí del Krita."

#: ../../reference_manual/tools/path.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/path.rst:11
msgid "Vector"
msgstr "Vectors"

#: ../../reference_manual/tools/path.rst:11
msgid "Path"
msgstr "Camí"

#: ../../reference_manual/tools/path.rst:11
msgid "Bezier Curve"
msgstr "Corbes de Bézier"

#: ../../reference_manual/tools/path.rst:11
msgid "Pen"
msgstr "Llapis"

#: ../../reference_manual/tools/path.rst:17
msgid "Bezier Curve Tool"
msgstr "Eina de corbes de Bézier"

#: ../../reference_manual/tools/path.rst:19
msgid "|toolbeziercurve|"
msgstr "|toolbeziercurve|"

#: ../../reference_manual/tools/path.rst:21
msgid ""
"You can draw curves by using this tool. Click the |mouseleft| to indicate "
"the starting point of the curve, then click again for consecutive control "
"points of the curve."
msgstr ""
"Podreu dibuixar les corbes utilitzant aquesta eina. Feu |mouseleft| per "
"indicar el punt d'inici de la corba, després feu clic de nou per obtenir els "
"punts de control consecutius de la corba."

#: ../../reference_manual/tools/path.rst:23
msgid ""
":program:`Krita` will show a blue line with two handles when you add a "
"control point. You can drag these handles to change the direction of the "
"curve in that point."
msgstr ""
"El :program:`Krita` mostrarà una línia blava amb dues nanses quan afegiu un "
"punt de control. Podreu arrossegar aquestes nanses per a canviar la direcció "
"de la corba en aquest punt."

#: ../../reference_manual/tools/path.rst:25
msgid ""
"On a vector layer, you can click on a previously inserted control point to "
"modify it. With an intermediate control point (i.e. a point that is not the "
"starting point and not the ending point), you can move the direction handles "
"separately to have the curve enter and leave the point in different "
"directions. After editing a point, you can just click on the canvas to "
"continue adding points to the curve."
msgstr ""
"Sobre una capa vectorial, podreu fer clic sobre un punt de control inserit "
"prèviament per a modificar-lo. Amb un punt de control intermedi (és a dir, "
"un punt que no serà el punt inicial ni el punt final), podreu moure les "
"nanses de direcció per separat perquè la corba entri i deixi el punt en "
"diferents direccions. Després d'editar un punt, podreu simplement fer clic "
"sobre el llenç per a continuar afegint punts a la corba."

#: ../../reference_manual/tools/path.rst:27
msgid ""
"Pressing the :kbd:`Del` key will remove the currently selected control point "
"from the curve. Double-click the |mouseleft| on any point of the curve or "
"press the :kbd:`Enter` key to finish drawing, or press the :kbd:`Esc` key to "
"cancel the entire curve. You can use the :kbd:`Ctrl` key while keeping the |"
"mouseleft| pressed to move the entire curve to a different position."
msgstr ""
"En prémer la tecla :kbd:`Supr` s'eliminarà el punt de control seleccionat de "
"la corba. Feu doble |mouseleft| sobre qualsevol punt de la corba o premeu la "
"tecla :kbd:`Retorn` per a finalitzar el dibuix, o premeu la tecla :kbd:`Esc` "
"per a cancel·lar tota la corba. Podeu utilitzar la tecla :kbd:`Ctrl` i "
"mentre la manteniu premuda per a moure tota la corba a una posició diferent."

#: ../../reference_manual/tools/path.rst:29
msgid ""
"While drawing the :kbd:`Ctrl` key while dragging will push the handles both "
"ways. The :kbd:`Alt` key will create a sharp corner, and the :kbd:`Shift` "
"key will allow you to make a handle while at the end of the curve. |"
"mouseright| will undo the last added point."
msgstr ""
"Amb la tecla :kbd:`Ctrl` mentre arrossegueu s'empenyeran les nanses en tots "
"dos sentits. La tecla :kbd:`Alt` crearà una cantonada més definida, i la "
"tecla :kbd:`Majús.` permetrà crear una nansa al final de la corba. Fer |"
"mouseright| desfarà l'últim punt afegit."

#: ../../reference_manual/tools/path.rst:32
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/path.rst:36
msgid "Autosmooth Curve"
msgstr "Corba de moviment suau"

#: ../../reference_manual/tools/path.rst:37
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"En alternar, els nodes s'inicialitzaran amb corbes suaus en lloc d'angles. "
"Desactiveu aquesta opció si voleu crear angles més definits per a un node. "
"Això no afectarà la nitidesa de la corba en arrossegar després de fer clic."

#: ../../reference_manual/tools/path.rst:39
msgid "Angle Snapping Delta"
msgstr "Delta de l'ajust de l'angle"

#: ../../reference_manual/tools/path.rst:40
msgid "The angle to snap to."
msgstr "L'angle al que s'ajustarà."

#: ../../reference_manual/tools/path.rst:42
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"L'ajust de l'angle farà que sigui més fàcil tenir la següent línia en un "
"angle específic de l'actual. L'angle estarà determinat pel :guilabel:`Delta "
"de l'ajust de l'angle`."
