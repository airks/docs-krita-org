.. meta::
   :description:
        Page about the quadratic blending modes in Krita: Freeze, Freeze-Reflect, Glow, Glow-Heat, Heat, Heat-Glow, Heat-Glow/Freeze-Reflect Hybrid, Reflect and Reflect-Freeze.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Reptorian <reptillia39@live.com>
   :license: GNU free documentation license 1.3 or later.

.. index:: Quadratic
.. _bm_cat_quadratric:

Quadratic
---------

.. versionadded:: 4.2

The quadratic blending modes are a set of modes intended to give various effects when adding light zones or overlaying shiny objects.

.. _bm_cat_freeze:

.. index:: ! Freeze Blending Mode

Freeze
~~~~~~

The freeze blending mode. Inversion of the reflect blending mode.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Freeze_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Freeze**.

.. _bm_cat_freeze_reflect:


Freeze-Reflect
~~~~~~~~~~~~~~

Mix of Freeze and Reflect blend mode.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Freeze_Reflect_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Freeze-Reflect**.

.. _bm_cat_glow:

Glow
~~~~

Reflect Blend Mode with source and destination layers swapped. 

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Glow_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Glow**.

.. _bm_cat_glow_heat:

Glow-Heat
~~~~~~~~~

Mix of Glow and Heat blend mode.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Glow_Heat_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Glow_Heat**.

.. _bm_cat_heat:

Heat
~~~~

The Heat Blend Mode. Inversion of the Glow Blend Mode.


.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Heat_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Heat**.

.. _bm_cat_heat_glow:

Heat-Glow
~~~~~~~~~

Mix of Heat, and Glow blending mode.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Heat_Glow_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Heat-Glow**.

.. _bm_cat_heat_glow_freeze_reflect:

Heat-Glow and Freeze-Reflect Hybrid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mix of the continuous quadratic blending modes. Very similar to overlay, and sometimes provides better result than overlay.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Heat_Glow_Freeze_Reflect_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Heat-Glow and Freeze-Reflect Hybrid**.

.. _bm_cat_reflect:

Reflect
~~~~~~~

Reflect is essentially Color Dodge Blending mode with quadratic falloff.


.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Reflect_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Reflect**.

.. _bm_cat_reflect_freeze:

Reflect-Freeze
~~~~~~~~~~~~~~

Mix of Reflect and Freeze blend mode.

.. figure:: /images/blending_modes/quadratic/Blending_modes_Q_Reflect_Freeze_Light_blue_and_Orange.png
   :align: center

   Left: **Normal**. Right: **Reflect-Freeze**.
    
